<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'nuvola');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '}kSiGXVUHGMY}-d<j.M3~Cz^{HA#* 0Pp}_L0LqOW/kKQ>OriyeTpec>Ge~)jnMG');
define('SECURE_AUTH_KEY',  '/K;ZIao[UP>kT.16sUZ|{;yfqa+{t,AGoZO+] V7{o,qE6qZ<$Z)1^Vas6[tIU]i');
define('LOGGED_IN_KEY',    '.PY8Q:Vu!kbI7hrg[GD:N%(T0I?B-u)K CZWsfE&ijfDG2}w:7b@si7oiy?:xRdB');
define('NONCE_KEY',        'WEE:38M~vyW| P!;jlp{*B4JRbTv r<7xcx~1V5:sU9GdD@PSnB7 TiCxEUB _Pm');
define('AUTH_SALT',        'CedzTdCrUOqdioWfi1;:uouD)wocpV;%l66ue[{DA3=6.;YbS%y2y8rS$T]ZT*|y');
define('SECURE_AUTH_SALT', 'iLRQ)FloDO49^QgBClh-8Pp^]vZw0~(L rurQ!!a%)KZi:1}M:6,|>J2Fc:a:Z]w');
define('LOGGED_IN_SALT',   '2`Xgq:KH#FK5u18xPX 9t$3DI.rxW_tq?6C(#l*{Ret[/hP&(Hv#go*>a;T^cA<1');
define('NONCE_SALT',       'HmH%@umT%TO{&tETxW2.4uo]D)/@b_Ukh^[dk%IpX9c@-:zKr{a@p[Mt-6[QE]N`');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
