��    	      d      �       �      �   d   �      Z  �   k  '     	   D     N  5   f  '  �     �  o   �     K  �   \  ,     	   F     P  5   h            	                                  Cheatin&#8217; huh? Hi! I'm here to assist you with re-ordering or disabling components of your theme's homepage design. Homepage Control No homepage components found. Please contact your theme author to make your theme compatible with Homepage Control or see this %sguide%s for details on how to do this yourself. Re-order the homepage components in %s. WooThemes http://woocommerce.com/ http://www.woocommerce.com/products/homepage-control/ PO-Revision-Date: 2018-03-25 15:21:46+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: GlotPress/2.4.0-alpha
Language: it
Project-Id-Version: Plugins - Homepage Control - Stable (latest release)
 Stai imbrogliando, eh? Ciao! Sono qui per assisterti nel riordinare o disabilitare componenti del design della home page del tuo tema. Homepage Control Nessun componente per la homepage. Contattare l'autore del tema per rendere il tema compatibile con il controllo Homepage oppure leggere questa  %sguide%s guida per i dettagli su come fare Ri-ordina i componenti della homepage in %s. WooThemes http://woocommerce.com/ http://www.woocommerce.com/products/homepage-control/ 