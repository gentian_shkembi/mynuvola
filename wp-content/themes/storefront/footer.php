<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package storefront
 */

?>

		</div><!-- .col-full -->
	</div><!-- #content -->

	<?php do_action( 'storefront_before_footer' ); ?>

	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="col-full">

			<?php
			/**
			 * Functions hooked in to storefront_footer action
			 *
			 * @hooked storefront_footer_widgets - 10
			 * @hooked storefront_credit         - 20
			 */
			do_action( 'storefront_footer' ); ?>

		</div><!-- .col-full -->
	</footer><!-- #colophon -->

	<?php do_action( 'storefront_after_footer' ); ?>

</div><!-- #page -->
<footer id="footer">
    <div class="center flex footer-content">
        <div>
            <h3>About</h3>
            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore
            magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis
            nisl ut aliquip ex ea commodo consequat.</p>
        </div>
        <div>
            <h3>Contact</h3>
            <ul>
                <li><strong>Address: </strong><a href="#">Tirana, Albania</a></li>
                <li><strong>Tel: </strong><a href="355696372756">0696372756</a></li>
                <li><strong>e-mail:</strong><a href="#">info@mynuvolamattress.com</a></li>
            </ul>
        </div>
        <div>
            <h3>Subscribe to our mailing list</h3>
            <form>
                <input type="text" placeholder="Email address" name="mail" >
                <input type="submit" value="Subscribe">
            </form>
        </div>
    </div>
</footer>
<?php wp_footer(); ?>

</body>
</html>
