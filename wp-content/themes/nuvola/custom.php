<?php
/**
 * The template for displaying full width pages.
 *
 * Template Name: Custom
 *
 * @package storefront
 */

get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

    <section class="page-header la-nuvola">
    </section>

    <?php if (has_post_thumbnail( $post->ID ) ): ?>
    <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>

        <section class="banner" style="background-image: url('<?php echo $image[0]; ?>')"></section>

    <?php endif; ?>

    <section class="page-spot">
        <div class="center">
            <h2>
            <?php echo __('
            [:en]A mattress designed so carefully it will make you feel like you are sleeping on a cloud!
            [:sq]Një dyshek i projektuar me kaq shumë kujdes sa do t\'ju bëjë të ndiheni sikur po flini mbi një re!
            '); 
            ?>
            </h2>
        </div>
    </section>

    <section class="slideShow vertical-margin">
        <div class="swiper-container">
            <div class="swiper-wrapper">
                <div class="swiper-slide">
                    <div class="mySwiper">
                        <img src="<?php echo get_stylesheet_directory_uri() ?>/images/icons/two.png" alt="">
                        <h3 class="swiper-title">
                        <?php echo __('
                        [:en]2 types of La Nuvola
                        [:sq]2 Lloje La Nuvola
                        '); 
                        ?>
                        </h3>
                        <p class="swiper-description">
                        <?php echo __('
                        [:en]La Nuvola gives you the possibility to select between two different equally luxurious & comfortable Mattresses
                        [:sq]La Nuvola ju jep mundësinë për të zgjedhur midis dy dyshekëve të ndryshëm, luksozë dhe të rehatshëm
                        '); 
                        ?>
                        </p>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="mySwiper">
                        <img src="<?php echo get_stylesheet_directory_uri() ?>/images/icons/76_mattress-orthopedic-support-back-spine-128.png" alt="">
                        <h3 class="swiper-title">
                        <?php echo __('
                        [:en]Zoned Comfort Memory Foam
                        [:sq]Memory Foam me 7 zona komforti
                        '); 
                        ?>
                        </h3>
                        <p class="swiper-description">
                        <?php echo __('
                        [:en]5cm of zoned comfort Visco Elastic Memory foam maximizing your comfort during sleep. The 5cm Memory Foam is crafted in such a way that it provides adequate support points for different parts of the body
                        [:sq]5cm Visco Elastic Memory foam, maksimizon komoditetin tuaj gjatë gjumit. Shtresat prej 5cm Memory Foam është përshtatur në menyrë të till për të ofruar mbështetje të vecantë për pjesë të ndyshme të trupit
                        '); 
                        ?>
                        </p>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="mySwiper">
                        <img src="<?php echo get_stylesheet_directory_uri() ?>/images/icons/76__mattress-orthopedic-spring-128.png" alt="">
                        <h3 class="swiper-title">
                        <?php echo __('
                        [:en]Pocket Spring System
                        [:sq]Susta Individuale
                        '); 
                        ?>
                        </h3>
                        <p class="swiper-description">
                        <?php echo __('
                        [:en]The pocket spring system provide ergonomic adaptability in line with the zoned comfort memory foam. Each spring is individually installed and clothed to proved altered support for different body areas
                        [:sq]Sustat individuale ofrojnë një përshtatje ergonomike me 7 zona të ndryshme memory foam. Çdo sustë është e instaluar dhe e mbështjellë individualisht, për të mundësuar mbështetjen në zona të ndryshme të trupit
                        '); 
                        ?>
                        </p>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="mySwiper">
                        <img src="<?php echo get_stylesheet_directory_uri() ?>/images/icons/76_mattress-orthopedic-bedding-air-circulation-128.png" alt="">
                        <h3 class="swiper-title">
                        <?php echo __('
                        [:en]Air System
                        [:sq]Sistem ajrimi
                        '); 
                        ?>
                        </h3>
                        <p class="swiper-description">
                        <?php echo __('
                        [:en]All layers allow maximum breathability so you do not overheat during sleep
                        [:sq]Të gjitha shtresat lejojnë frymëmarrje maksimale, në mënyrë që ju të ruani temperaturën optimale gjatë gjumit
                        '); 
                        ?>
                        </p>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="mySwiper">
                        <img src="<?php echo get_stylesheet_directory_uri() ?>/images/icons/76_mattress-dust-mite-anti-128.png" alt="">
                        <h3 class="swiper-title">
                        <?php echo __('
                        [:en]Antiallergic & Antibacterial
                        [:sq]Antialergjik & Antibakterial
                        '); 
                        ?>
                        </h3>
                        <p class="swiper-description">
                        <?php echo __('
                        [:en]All materials selected are Antiallergic and Antibacterial. You can sleep safe knowing that your allergies or different bacteria will not interfere with your good night sleep.
                        [:sq]I veshur me materiale të përzgjedhura posacërisht për të mos shkaktuar asnjë lloj alergjie dhe për të evituar prezencën e bakterieve. Ju mund ta bëni gjumin e qetë duke e ditur se alergjitë dhe bakteriet nuk do ju shqetësojnë gjatë natës.
                        '); 
                        ?>
                        </p>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="mySwiper">
                        <img src="<?php echo get_stylesheet_directory_uri() ?>/images/icons/76_mattress-orthopedic-bedding-furniture-16-128.png" alt="">
                        <h3 class="swiper-title">
                        <?php echo __('
                        [:en]Cashmere Cover
                        [:sq]Mbulesë Kashmiri
                        '); 
                        ?>
                        </h3>
                        <p class="swiper-description">
                        <?php echo __('
                        [:en]Cashmere it\'s known for his extreme softnesd and luxurious quality. The insulating properties of Cashmir make it ideal for all seasons throughout the year
                        [:sq]Është i njohur për butësinë e tij ekstreme dhe cilësitë luksoze. Vetitë izoluese të kashmirit e bëjnë atë ideal për të gjitha stinët e vitit
                        '); 
                        ?>
                        </p>
                    </div>
                </div>
            </div>
            <!-- Add Pagination -->
            <div class="swiper-pagination"></div>
            <!-- Add Arrows -->
            <div class="swiper-button-next"></div>
            <div class="swiper-button-prev"></div>
        </div>
    </section>

    <section class="gallery vertical-margin">
        <h3 class="header-style">
            <?php echo __('
            [:en]Gallery
            [:sq]Galeri
            [:it]Galleria
            [:de]Galerie
            '); 
            ?>
        </h3>
        <?php echo do_shortcode('[supsystic-gallery id=1]') ?>
    </section>

    <!-- product details -->
    <section class="product-details vertical-margin">

        <div class="center">
            <h3 class="header-style">
                <?php echo __('
                [:en]Product Specs
                [:sq]Specifikimet e Produktit
                [:it]Specifiche del Prodotto
                [:de]Produktspezifikationen
                '); 
                ?>
            </h3>
            <!-- product specs -->
            <article>
                <div class="seeMore-header">
                    <?php echo __('
                    [:en]Size
                    [:sq]Përmasa
                    [:it]Dimensione
                    [:de]Größe
                    '); 
                    ?>
                    <span>+</span>
                </div>
                <div class="flex spec">
                    <table>
                        <thead>
                            <tr>
                                <th colspan="2">Twin</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <?php echo __('
                                    [:en]Width
                                    [:sq]Gjerësi
                                    [:it]Larghezza
                                    [:de]Breite
                                    '); 
                                    ?>
                                </td>
                                <td>90cm</td>
                            </tr>
                            <tr>
                                <td>
                                    <?php echo __('
                                    [:en]Length
                                    [:sq]Gjatësi
                                    [:it]Lunghezza
                                    [:de]Länge
                                    '); 
                                    ?>
                                </td>
                                <td>190cm</td>
                            </tr>
                            <tr>
                                <td>
                                    <?php echo __('
                                    [:en]Height
                                    [:sq]Lartësi
                                    [:it]Altezza
                                    [:de]Höhe
                                    '); 
                                    ?>
                                </td>
                                <td>30cm</td>
                            </tr>
                        </tbody>
                    </table>
                    <table>
                        <thead>
                            <tr>
                                <th colspan="2">King</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <?php echo __('
                                    [:en]Width
                                    [:sq]Gjerësi
                                    [:it]Larghezza
                                    [:de]Breite
                                    '); 
                                    ?>
                                </td>
                                <td>160cm</td>
                            </tr>
                            <tr>
                                <td>
                                    <?php echo __('
                                    [:en]Length
                                    [:sq]Gjatësi
                                    [:it]Lunghezza
                                    [:de]Länge
                                    '); 
                                    ?>
                                </td>
                                <td>190cm</td>
                            </tr>
                            <tr>
                                <td>
                                    <?php echo __('
                                    [:en]Height
                                    [:sq]Lartësi
                                    [:it]Altezza
                                    [:de]Höhe
                                    '); 
                                    ?>
                                </td>
                                <td>30cm</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </article>
            <article>
                <div class="seeMore-header">
                    <?php echo __('
                    [:en]Materials
                    [:sq]Materiale
                    [:it]Materiale
                    [:de]Materialien
                    '); 
                    ?>
                    <span>+</span></div>
                <div class="flex spec">
                    <table>
                        <thead>
                            <tr>
                                <th colspan="2">La Nuvola Foam</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <img src="<?php echo get_stylesheet_directory_uri() ?>/images/catalog/la-nuvola-foam.png" alt="La Nuvola Foam">
                                </td>
                            </tr>
                        </tbody>
                        <!-- <tbody>
                            <tr>
                                <td>
                                <?php echo __('
                                [:en]Cashmere Cover
                                [:sq]I veshur me mbulesë Kashmiri
                                '); 
                                ?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                <?php echo __('
                                [:en]Polyester stitched to Cover
                                [:sq]Shtresë mbrojtëse prej poliestre
                                '); 
                                ?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                <?php echo __('
                                [:en]5 cm Visco-Elastic Memory Foam
                                [:sq]5 cm Visco-Elastic Memory Foam
                                '); 
                                ?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                <?php echo __('
                                [:en]25 cm High Density Foam
                                [:sq]25 cm sfungjer me densitet të lartë
                                '); 
                                ?>
                                </td>
                            </tr>
                        </tbody> -->
                    </table>
                    <table>
                        <thead>
                            <tr>
                                <th colspan="2">La Nuvola Pocket Spring System</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <img src="<?php echo get_stylesheet_directory_uri() ?>/images/catalog/la-nuvola-pocket-spring.png" alt="La Nuvola Pocket Spring">
                                </td>
                            </tr>
                        </tbody>
                        <!-- <tbody>
                            <tr>
                                <td>
                                <?php echo __('
                                [:en]Cashmere Cover
                                [:sq]I veshur me mbulesë Kashmiri
                                '); 
                                ?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                <?php echo __('
                                [:en]Polyester stitched to Cover
                                [:sq]Shtresë mbrojtëse prej poliestre
                                '); 
                                ?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                <?php echo __('
                                [:en]5 cm Visco-Elastic Memory Foam
                                [:sq]5 cm Visco-Elastic Memory Foam
                                '); 
                                ?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                <?php echo __('
                                [:en]5 cm High Density Foam
                                [:sq]5 cm sfungjer me densitet të lartë
                                '); 
                                ?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                <?php echo __('
                                [:en]15 cm Pocked Spring System
                                [:sq]15 cm Susta individuale
                                '); 
                                ?>
                                </td>
                            </tr>
                        </tbody> -->
                    </table>
                </div>
            </article>
            <article>
                <div class="seeMore-header">
                    <?php echo __('
                    [:en]Warranty
                    [:sq]Garanci
                    [:it]Garanzia
                    [:de]Garantie
                    '); 
                    ?>
                    <span>+</span>
                </div>
                <div class="spec">
                    <?php echo __('
                    [:en]15 years limited warranty <a href="#">learn more</a> 
                    [:sq]15 vjet garanci <a href="#">mëso më shumë</a> 
                    '); 
                    ?>
                </div>
            </article>
            <article>
                <div class="seeMore-header">
                    <?php echo __('
                    [:en]Price
                    [:sq]Çmimi
                    [:it]Prezzo
                    [:de]Preis
                    '); 
                    ?>
                    <span>+</span>
                </div>
                <div class="flex spec">
                    <table>
                        <thead>
                            <tr>
                                <th colspan="2">La Nuvola Foam</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Twin 89.900 ALL</td>
                            </tr>
                            <tr>
                                <td>King 159.000 ALL</td>
                            </tr>
                        </tbody>
                    </table>
                    <table>
                        <thead>
                            <tr>
                                <th colspan="2">La Nuvola Pocket Spring System</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Twin 89.900 ALL</td>
                            </tr>
                            <tr>
                                <td>King 159.000 ALL</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </article>
            <!-- product specs end -->
        </div>

    </section>
    <!-- product detalis end -->

<?php endwhile; // end of the loop. ?>

<?php
get_footer();
