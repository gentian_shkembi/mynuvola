<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package storefront
 */

?>

		</div><!-- .col-full -->
	</div><!-- #content -->

	<?php do_action( 'storefront_before_footer' ); ?>

	<footer id="footer">
        <div class="center flex footer-content">
            <div>
                <h3>Social</h3>
                <ul>
                    <li><a target="_blank" rel="noopener noreferrer" href="https://www.facebook.com/mynuvolamattress/"><i class="fab fa-facebook"></i>Facebook</a></li>
                    <li><a target="_blank" rel="noopener noreferrer" href="https://www.instagram.com/mynuvolamattress/"><i class="fab fa-instagram"></i>Instagram</a></li>
                </ul>
            </div>
            <div>
                <h3>
                <?php echo __('
                [:en]Contact
                [:sq]Kontakt
                [:it]Contatto
                [:de]Kontakt
                '); 
                ?>
                </h3>
                <ul>
                    <li>
                        <strong>
                        <?php echo __('
                        [:en]Address:
                        [:sq]Adresa:
                        [:it]Indirizzo:
                        [:de]Adres:
                        '); 
                        ?>
                        </strong>
                        <a href="#">Tirana, Albania</a>
                    </li>
                    <li><strong>Tel: </strong><a href="tel:+355697873396">+355 69 78 73 396</a></li>
                    <li><strong>e-mail:</strong><a href="#">info@mynuvolamattress.com</a></li>
                </ul>
            </div>
            <div>
                <h3>
                <?php echo __('
                [:en]Subscribe to our mailing list
                [:sq]Regjistrohu në listën tonë të postimeve
                [:it]Iscriviti alla nostra mailing list
                [:de]Abonnieren Sie unsere Mailing-Liste
                '); 
                ?>
                </h3>
                <form>
                    <input type="text" placeholder="Email address" name="mail" >
                    <input type="submit" value="Subscribe">
                </form>
            </div>
        </div>
        <div class="copyright">
            &copy; Nuvola <?php echo date("Y"); ?>
        </div>
    </footer>
    <?php wp_footer(); ?>

</div><!-- #page -->

<script src="<?php echo get_stylesheet_directory_uri() ?>/js/swiper.min.js"></script>

</body>
</html>