jQuery(document).ready(function () {

    var fullDate = new Date();
    var hour = fullDate.getHours();

    // if (hour >= 19) {
    //     jQuery('.sky').removeClass('day');
    //     jQuery('.sky').addClass('night');
    // } else {
    //     jQuery('.sky').removeClass('night');
    //     jQuery('.sky').addClass('day');
    // }

    jQuery('.seeMore-header').click(function () {
        var thisI = jQuery(this);
        var child = thisI.children('span');
        var sibling = thisI.next();
        var parent = thisI.parent();

        // thisI.addClass('active');

        if (thisI.hasClass('active')) {
            thisI.removeClass('active');
            child.css('transform', 'rotate(0deg)');
            sibling.slideUp(200);
        } else {
            thisI.addClass('active');
            child.css('transform', 'rotate(45deg)');
            sibling.slideDown(200);
        }

        parent.siblings().find('span').css('transform', 'rotate(0deg)');
        parent.siblings().find('.spec').slideUp(200);
        parent.siblings().find('.seeMore-header').removeClass('active');
    });

    jQuery('.mobile-menu-btn i').click(function () {
        jQuery('.handheld-navigation').slideToggle(200);
    });

    jQuery('.handheld-navigation a').click(function () {
        jQuery(this).siblings('.sub-menu').slideToggle(150);
    });

});

function showPage() {
    document.getElementById("loader").style.display = "none";
    document.getElementById("page").style.display = "block";
}

jQuery(window).on('load', function () {
    showPage();

    var swiper = new Swiper('.swiper-container', {
        slidesPerView: 1,
        spaceBetween: 30,
        loop: true,
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        },
        autoplay: {
            delay: 3000,
        },
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
    });
});