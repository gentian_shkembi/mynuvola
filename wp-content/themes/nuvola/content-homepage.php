<?php
/**
 * The template used for displaying page content in template-homepage.php
 *
 * @package storefront
 */

?>


<?php
$featured_image = get_the_post_thumbnail_url( get_the_ID(), 'thumbnail' );
?>


<div id="post-<?php the_ID(); ?>" <?php post_class(); ?> style="<?php storefront_homepage_content_styles(); ?>" data-featured-image="<?php echo $featured_image; ?>">
	<div class="col-full">
		<?php
		/**
		 * Functions hooked in to storefront_page add_action
		 *
		 * @hooked storefront_homepage_header      - 10
		 * @hooked storefront_page_content         - 20
		 */
		do_action( 'storefront_homepage' );
		?>
	</div>
</div><!-- #post-## -->

<div class="sky">
	<img src="<?php echo get_stylesheet_directory_uri() ?>/images/nuvola-logo-white.png" alt="Nuvola Logo Banner">
	<!-- <video class="video" width="320" height="240" autoplay loop muted>
		<source src="<?php echo get_stylesheet_directory_uri() ?>/videos/nuvola-afternoon.mp4" type="video/mp4">
		Your browser does not support the video tag.
	</video> -->

	<!-- <img class="cloud cloud-1" src="<?php //echo get_stylesheet_directory_uri() ?>/images/cloud.png" alt="cloud">
	<img class="cloud cloud-2" src="<?php //echo get_stylesheet_directory_uri() ?>/images/cloud.png" alt="cloud">
	<img class="cloud cloud-3" src="<?php //echo get_stylesheet_directory_uri() ?>/images/cloud.png" alt="cloud">
	<img class="cloud cloud-4" src="<?php //echo get_stylesheet_directory_uri() ?>/images/cloud.png" alt="cloud">
	<img class="cloud cloud-5" src="<?php //echo get_stylesheet_directory_uri() ?>/images/cloud.png" alt="cloud">
	<img class="cloud cloud-6" src="<?php //echo get_stylesheet_directory_uri() ?>/images/cloud.png" alt="cloud">
	<div class="mattress-banner">
		<img class="float" src="<?php //echo get_stylesheet_directory_uri() ?>/images/home/home-page.png" alt="">
	</div>

	<div class="stars"></div>
	<div class="twinkling"></div> -->

</div>

<section class="page-spot">
	<div class="center">
		<h2>
		<?php echo __('
		[:en]We are bound to bring you the comfort of a high quality mattress
		[:sq]Jemi të dedikuar të sjellim te ju komoditetin e një dysheku të cilësisë së lartë
		[:it]Siamo decisi a portare da voi il comfort di un  materasso di alta qualità
		'); 
		?>
		</h2>
	</div>
</section>

<ul class="mattress">
	<li class="mattress-type">
		<a href="nuvola" style="background-image: url(<?php echo get_stylesheet_directory_uri() ?>/images/home/nuvola.JPG); background-position: center; background-repeat: no-repeat; background-size: cover">
			<div class="text">
				<div>
					<h3>La Nuvola</h3>
					<p>
					<?php echo __('
					[:en]A mattress designed so carefully it will make you feel like you are sleeping on a cloud!
					[:sq]Një dyshek i projektuar me kaq shumë kujdes sa do t\'ju bëjë të ndiheni sikur po flini mbi një re!
					'); 
					?>
					</p>
				</div>
			</div>
		</a>
	</li>
	<li class="mattress-type">
		<a href="somnus" style="background-image: url(<?php echo get_stylesheet_directory_uri() ?>/images/home/somnus.JPG); background-position: center; background-repeat: no-repeat; background-size: cover">
			<div class="text">
				<div>
					<h3>Somnus</h3>
					<p>
					<?php echo __('
					[:en]The GOD of SLEEP
					[:sq]Perëndia e GJUMIT
					'); 
					?>
					</p>
				</div>
			</div>
		</a>
	</li>
	<li class="mattress-type">
		<a href="sky" style="background-image: url(<?php echo get_stylesheet_directory_uri() ?>/images/home/sky.jpg); background-position: center; background-repeat: no-repeat; background-size: cover">
			<div class="text">
				<div>
					<h3>Sky</h3>
					<p>
					<?php echo __('
					[:en]Sometime LESS is MORE
					[:sq]Ndonjeherë më pak është MË SHUMË
					'); 
					?>
					</p>
				</div>
			</div>
		</a>
	</li>
</ul>
