<?php
/**
 * The template for displaying full width pages.
 *
 * Template Name: Somnus
 *
 * @package storefront
 */

get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

    <section class="page-header la-nuvola">
    </section>

    <?php if (has_post_thumbnail( $post->ID ) ): ?>
    <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>

        <section class="banner" style="background-image: url('<?php echo $image[0]; ?>')"></section>
        
    <?php endif; ?>

    <section class="page-spot">
        <div class="center">
            <h2>
            <?php echo __('
            [:en]SOMNUS the GOD of SLEEP
            [:sq]SOMNUS perëndia e GJUMIT
            '); 
            ?>
            </h2>
        </div>
    </section>

    <section class="slideShow vertical-margin">
        <div class="swiper-container">
            <div class="swiper-wrapper">
                <div class="swiper-slide">
                    <div class="mySwiper">
                        <img src="<?php echo get_stylesheet_directory_uri() ?>/images/icons/aloe-vera.png" alt="">
                        <h3 class="swiper-title">
                        <?php echo __('
                        [:en]Aloe Vera
                        [:sq]Aloe Vera
                        [:it]Aloe Vera
                        [:de]Aloe Vera
                        '); 
                        ?>
                        </h3>
                        <p class="swiper-description">
                        <?php echo __('
                        [:en]Multilayer Aloe Vera Infused materials. Aloe Vera is known not only for its medicinal qualities, but also other natural properties that have been used to heal and protect the human body for centuries
                        [:sq]Shtresa materialesh të injektuar me Aloe Vera. Është e njohur jo vetëm për cilësitë e saj medicinale, por edhe për vetitë e tjera natyrore që janë përdorur për të shëruar dhe mbrojtur trupin e njeriut për shekuj me rradhë
                        '); 
                        ?>
                        </p>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="mySwiper">
                        <img src="<?php echo get_stylesheet_directory_uri() ?>/images/icons/76_mattress-orthopedic-support-back-spine-128.png" alt="">
                        <h3 class="swiper-title">
                        <?php echo __('
                        [:en]Zoned Comfort
                        [:sq]Memory foam me 7 zona komforti
                        '); 
                        ?>
                        </h3>
                        <p class="swiper-description">
                        <?php echo __('
                        [:en]4cm of zoned comfort Visco Elastic Memory foam maximizing your comfort during sleep. The 4cm Memory Foam is crafted in such a way that it provides adequate support points for different parts of the body
                        [:sq]4cm Visco Elastic Memory foam, maksimizon komoditetin tuaj gjatë gjumit. Shtresat prej 4cm Memory Foam është përshtatur në menyrë të till për të ofruar mbështetje të vecantë për pjesë të ndyshme të trupit
                        '); 
                        ?>
                        </p>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="mySwiper">
                        <img src="<?php echo get_stylesheet_directory_uri() ?>/images/icons/76_mattress-orthopedic-bedding-air-circulation-128.png" alt="">
                        <h3 class="swiper-title">
                        <?php echo __('
                        [:en]Airing system
                        [:sq]Sistem ajrimi
                        '); 
                        ?>
                        </h3>
                        <p class="swiper-description">
                        <?php echo __('
                        [:en]All layers allow maximum breathability so you do not overheat during sleep
                        [:sq]Të gjitha shtresat lejojnë frymëmarrje maksimale, në mënyrë që ju të ruani temperaturën optimale gjatë gjumit
                        '); 
                        ?>
                        </p>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="mySwiper">
                        <img src="<?php echo get_stylesheet_directory_uri() ?>/images/icons/76_mattress-dust-mite-anti-128.png" alt="">
                        <h3 class="swiper-title">
                        <?php echo __('
                        [:en]Antiallergic & Antibacterial
                        [:sq]Antialergjik & Antibakterial
                        '); 
                        ?>
                        </h3>
                        <p class="swiper-description">
                        <?php echo __('
                        [:en]All materials selected are Antiallergic and Antibacterial. You can sleep safe knowing that your allergies or different bacteria will not interfere with your good night sleep
                        [:sq]I veshur me materiale të përzgjedhura posacërisht për të mos shkaktuar asnjë lloj alergjie dhe për të evituar prezencën e bakterieve. Ju mund ta bëni gjumin e qetë duke e ditur se alergjitë dhe bakteriet nuk do ju shqetësojnë gjatë natës
                        '); 
                        ?>
                        </p>
                    </div>
                </div>
            </div>
            <!-- Add Pagination -->
            <div class="swiper-pagination"></div>
            <!-- Add Arrows -->
            <div class="swiper-button-next"></div>
            <div class="swiper-button-prev"></div>
        </div>
    </section>

    <section class="gallery vertical-margin">
        <h3 class="header-style">
            <?php echo __('
            [:en]Gallery
            [:sq]Galeri
            [:it]Galleria
            [:de]Galerie
            '); 
            ?>
        </h3>
        <?php echo do_shortcode('[supsystic-gallery id=2]') ?>
    </section>

    <!-- product details -->
    <section class="product-details vertical-margin">

        <div class="center">
            <h3 class="header-style">
                <?php echo __('
                [:en]Product Specs
                [:sq]Specifikimet e Produktit
                [:it]Specifiche del Prodotto
                [:de]Produktspezifikationen
                '); 
                ?>
            </h3>
            <!-- product specs -->
            <article>
                <div class="seeMore-header">
                    <?php echo __('
                    [:en]Size
                    [:sq]Përmasa
                    [:it]Dimensione
                    [:de]Größe
                    '); 
                    ?>
                    <span>+</span>
                </div>
                <div class="flex spec">
                    <table>
                        <thead>
                            <tr>
                                <th colspan="2">Twin</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <?php echo __('
                                    [:en]Width
                                    [:sq]Gjerësi
                                    [:it]Larghezza
                                    [:de]Breite
                                    '); 
                                    ?>
                                </td>
                                <td>90cm</td>
                            </tr>
                            <tr>
                                <td>
                                    <?php echo __('
                                    [:en]Length
                                    [:sq]Gjatësi
                                    [:it]Lunghezza
                                    [:de]Länge
                                    '); 
                                    ?>
                                </td>
                                <td>190cm</td>
                            </tr>
                            <tr>
                                <td>
                                    <?php echo __('
                                    [:en]Height
                                    [:sq]Lartësi
                                    [:it]Altezza
                                    [:de]Höhe
                                    '); 
                                    ?>
                                </td>
                                <td>25cm</td>
                            </tr>
                        </tbody>
                    </table>
                    <table>
                        <thead>
                            <tr>
                                <th colspan="2">King</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <?php echo __('
                                    [:en]Width
                                    [:sq]Gjerësi
                                    [:it]Larghezza
                                    [:de]Breite
                                    '); 
                                    ?>
                                </td>
                                <td>160cm</td>
                            </tr>
                            <tr>
                                <td>
                                    <?php echo __('
                                    [:en]Length
                                    [:sq]Gjatësi
                                    [:it]Lunghezza
                                    [:de]Länge
                                    '); 
                                    ?>
                                </td>
                                <td>190cm</td>
                            </tr>
                            <tr>
                                <td>
                                    <?php echo __('
                                    [:en]Height
                                    [:sq]Lartësi
                                    [:it]Altezza
                                    [:de]Höhe
                                    '); 
                                    ?>
                                </td>
                                <td>25cm</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </article>
            <article>
                <div class="seeMore-header">
                    <?php echo __('
                    [:en]Materials
                    [:sq]Materiale
                    [:it]Materiale
                    [:de]Materialien
                    '); 
                    ?>
                    <span>+</span></div>
                <div class="flex spec">
                    <table>
                        <thead>
                            <tr>
                                <th colspan="2">Somnus</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <img src="<?php echo get_stylesheet_directory_uri() ?>/images/catalog/somnus.png" alt="Somnus">
                                </td>
                            </tr>
                        </tbody>
                        <!-- <tbody>
                            <tr>
                                <td>
                                <?php echo __('
                                [:en]Aloe Vera Cover
                                [:sq]Mbulesë Aloe Vera
                                '); 
                                ?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                <?php echo __('
                                [:en]Polyester stitched to Cover
                                [:sq]Shtresë mbrojtëse prej poliestre
                                '); 
                                ?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                <?php echo __('
                                [:en]4 cm Visco-Elastic Memory Foam
                                [:sq]4 cm Visco-Elastic Memory Foam
                                '); 
                                ?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                <?php echo __('
                                [:en]21 cm High Density Foam
                                [:sq]21 cm sfungjer me densitet të lartë
                                '); 
                                ?>
                                </td>
                            </tr>
                        </tbody> -->
                    </table>
                </div>
            </article>
            <article>
                <div class="seeMore-header">
                    <?php echo __('
                    [:en]Warranty
                    [:sq]Garanci
                    [:it]Garanzia
                    [:de]Garantie
                    '); 
                    ?>
                    <span>+</span>
                </div>
                <div class="spec">
                    <?php echo __('
                    [:en]12 years limited warranty <a href="#">learn more</a> 
                    [:sq]12 vjet garanci <a href="#">mëso më shumë</a> 
                    '); 
                    ?>
                </div>
            </article>
            <article>
                <div class="seeMore-header">
                    <?php echo __('
                    [:en]Price
                    [:sq]Çmimi
                    [:it]Prezzo
                    [:de]Preis
                    '); 
                    ?>
                    <span>+</span>
                </div>
                <div class="flex spec">
                    <table>
                        <thead>
                            <tr>
                                <th colspan="2">Somnus</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Twin 49.900 ALL</td>
                            </tr>
                            <tr>
                                <td>King 89.000 ALL</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </article>
            <!-- product specs end -->
        </div>

    </section>
    <!-- product detalis end -->

<?php endwhile; // end of the loop. ?>

<?php
get_footer();
