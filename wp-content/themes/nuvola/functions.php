<?php

// local configuration
// update_option( 'siteurl', 'http://localhost/webx/nuvola/' );
// update_option( 'home', 'http://localhost/webx/nuvola/' );

// server configuration
// update_option( 'siteurl', 'https://www.mynuvolamattress.com/' );
// update_option( 'home', 'https://www.mynuvolamattress.com/' );

add_action( 'wp_enqueue_scripts', 'enqueue_parent_theme_style');
function enqueue_parent_theme_style() {
    wp_enqueue_style( 'parent-style', get_template_directory_uri().'/style.css' );
}

function my_scripts_method() {
    wp_enqueue_script(
        'custom-script',
        get_stylesheet_directory_uri() . '/script.js',
        array( 'jquery' )
    );
}

add_action( 'wp_enqueue_scripts', 'my_scripts_method' );

add_theme_support('post-thumbnails');

?>