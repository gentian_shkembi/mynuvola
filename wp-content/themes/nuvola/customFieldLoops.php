<?php
/**
 * The template for displaying full width pages.
 *
 * Template Name: CustomFieldLoops
 *
 * @package storefront
 */

get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

    <section class="page-header la-nuvola">
        <h1>La Nuvola</h1>
    </section>

    <section class="banner" style="background-image: url('<?php echo get_stylesheet_directory_uri() ?>/images/bedroom.jpg')"></section>

    <section class="page-spot center">
        <h2>A mattress designed so carefully it will make you feel like you are sleeping on a cloud!</h2>
    </section>

    <section class="slideShow">
        <div class="swiper-container">
            <div class="swiper-wrapper">
                <div class="swiper-slide">
                    <div class="mySwiper">
                        <img src="<?php echo get_stylesheet_directory_uri() ?>/images/icons/two.svg" alt="">
                        <h3 class="swiper-title">2 types of La Nuvola</h3>
                        <p class="swiper-description">La Nuvola gives you the possibility To select between Two different equally Luxurious & comfortable Matresses.</p>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="mySwiper">
                        <img src="<?php echo get_stylesheet_directory_uri() ?>/images/icons/two.svg" alt="">
                        <h3 class="swiper-title">Zoned Comfort Memory Foam</h3>
                        <p class="swiper-description">5Cm of zoned comfort Visco Elastic Memory foam maximizing your comfort during sleep</p>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="mySwiper">
                        <img src="<?php echo get_stylesheet_directory_uri() ?>/images/icons/two.svg" alt="">
                        <h3 class="swiper-title">Pocket Spring System</h3>
                        <p class="swiper-description">The pocket spring system provides ergonomic adaptability in line with the zoned comfort memory foam. Each spring is individually installed and clothed to proved altered support for different body areas</p>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="mySwiper">
                        <img src="<?php echo get_stylesheet_directory_uri() ?>/images/icons/two.svg" alt="">
                        <h3 class="swiper-title">Air System</h3>
                        <p class="swiper-description">All layers allow maximum breath ability so you do not overheat during sleep</p>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="mySwiper">
                        <img src="<?php echo get_stylesheet_directory_uri() ?>/images/icons/two.svg" alt="">
                        <h3 class="swiper-title">Antiallergic & Antibacterial</h3>
                        <p class="swiper-description">All materials selected are Antialergic and Antibacterial</p>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="mySwiper">
                        <img src="<?php echo get_stylesheet_directory_uri() ?>/images/icons/two.svg" alt="">
                        <h3 class="swiper-title">Cashmir Cover</h3>
                        <p class="swiper-description">Its Known for his extream softnes and luxurious quality. The insulating propertys of Cashmir make it ideal for all seasons threw the year</p>
                    </div>
                </div>
            </div>
            <!-- Add Pagination -->
            <div class="swiper-pagination"></div>
            <!-- Add Arrows -->
            <div class="swiper-button-next"></div>
            <div class="swiper-button-prev"></div>
        </div>
    </section>

    <section class="gallery">
        <h3 class="header-style">Gallery</h3>
        <?php echo do_shortcode('[supsystic-gallery id=4]') ?>
    </section>

    <!-- product details -->
    <section class="product-details">

        <!-- product specs -->
        <article class="product-specs center">
            <div class="seeMore-header">Product Specs <span>+</span></div>
            <div class="flex">
                <table>
                    <thead>
                        <tr>
                            <th colspan="2">Twin</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Gjerësi</td>
                            <td>90cm</td>
                        </tr>
                        <tr>
                            <td>Gjatësi</td>
                            <td>190cm</td>
                        </tr>
                        <tr>
                            <td>Lartësi</td>
                            <td>30cm</td>
                        </tr>
                    </tbody>
                </table>
                <table>
                    <thead>
                        <tr>
                            <th colspan="2">King</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Gjerësi</td>
                            <td>160cm</td>
                        </tr>
                        <tr>
                            <td>Gjatësi</td>
                            <td>190cm</td>
                        </tr>
                        <tr>
                            <td>Lartësi</td>
                            <td>30cm</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </article>
        <!-- product specs end -->

    </section>
    <!-- product detalis end -->

    <!-- get custom field -->
    <p>Custom field from plugin: <?php echo get_post_meta($post->ID, 'length', true); ?></p>

    <!-- get and check if custom field exist -->
    <?php 
    
        $length = get_post_meta($post->ID, 'length', true);

        if ($length) { ?>

        <p>The length is: <?php echo $length; ?></p>

        <?php 

        } else { 
        // do nothing; 
        }
    
    ?>

    <!-- get and check if there is more than one value -->
    <?php
        $length = get_post_meta($post->ID, 'length', false);
        if( count( $length ) != 0 ) { ?>
        <p>List of length:</p>
        <ul>
        <?php foreach($length as $length) {
                    echo '<li>'.$length.'</li>';
                    }
                    ?>
        </ul>
        <?php 
        } else { 
        // do nothing; 
        }
    ?>

<?php endwhile; // end of the loop. ?>

<?php
get_footer();
